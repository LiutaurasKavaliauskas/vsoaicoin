import json

from datetime import date


def serialize(object):
    """
    Given the object serialize it to the dictionary

    :param object:
    :return:
    """
    if isinstance(object, date):
        serial = object.isoformat()
        return serial

    return object.__dict__


def serialize_array(objects):
    """
    Given the objects serialize them to json

    :param objects:
    :return:
    """
    objects_json = []

    for object in objects:
        objects_json.append(json.dumps(object, default=serialize))

    return objects_json
