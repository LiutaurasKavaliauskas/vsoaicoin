import base64
import hashlib
from argparse import ArgumentParser
from uuid import uuid4

import jwt
from flask import Flask, jsonify, request, render_template
from flask_cors import CORS

import serializer
from account import Account
from blockchain import Blockchain
from insufficient_funds_exception import InsufficientFundsException
from wallet import Wallet

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})

node_identifier = str(uuid4()).replace('-', '')

blockchain = Blockchain()


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/generate', methods=['GET'])
def generate_new_wallet():
    priv_key = Wallet.generate_private_key()
    pub_key = Wallet.generate_public_key(priv_key)

    return render_template('generated.html', pub_key=pub_key, priv_key=priv_key)


@app.route('/balance', methods=['POST'])
def balance():
    return jsonify(serializer.serialize(Account.fetch(blockchain, request.args.get('public_key')))), 200


@app.route('/block/new', methods=['POST'])
def new_block():
    block = blockchain.new_block(request.form['proof'], request.form['recipient'])

    return jsonify({
        'index': block.index,
        'proof': block.proof
    }), 200


@app.route('/transactions/new', methods=['POST'])
def new_transaction():
    """
    An endpoint for the new transaction creation

    :return: json
    """
    encoded = request.form['request']

    encoded_public_key = jwt.get_unverified_header(encoded)['public_key']
    pub_key = '-----BEGIN PUBLIC KEY-----' + base64.b64decode(encoded_public_key).decode(
        'utf-8') + '-----END PUBLIC KEY-----'

    decoded = jwt.decode(encoded, pub_key, algorithms='RS256')

    sender = hashlib.sha256(encoded_public_key.encode('utf-8')).hexdigest()

    try:
        index = blockchain.new_transaction(sender, decoded['address'], decoded['amount'])
    except InsufficientFundsException as error:
        return jsonify({
            'message': f'{error.message}',
        }), 403

    return jsonify({
        'message': f'Transaction will be added to Block {index}',
        'current_transactions': serializer.serialize_array(blockchain.current_transactions),
        'chain': serializer.serialize_array(blockchain.chain),
    }), 201


@app.route('/chain', methods=['GET'])
def full_chain():
    """
    An endpoint for the chain receiving

    :return: json
    """
    return jsonify({
        'chain': serializer.serialize_array(blockchain.chain),
        'length': len(blockchain.chain),
    }), 200


@app.route('/nodes/register', methods=['POST'])
def register_nodes():
    """
    An endpoint to register new nodes

    :return: json
    """
    nodes = request.get_json().get('nodes')

    if nodes is None:
        return "Error: Please supply a valid list of nodes", 400

    for node in nodes:
        blockchain.register_node(node)

    response = {
        'message': 'New nodes have been added',
        'total_nodes': list(blockchain.nodes),
    }

    return jsonify(response), 201


@app.route('/nodes/resolve', methods=['GET'])
def consensus():
    """
    An endpoint to resolve the blockchains among nodes

    :return: json
    """
    replaced = blockchain.resolve_conflicts()

    if replaced:
        response = {
            'message': 'Our chain was replaced',
            'new_chain': serializer.serialize_array(blockchain.chain)
        }
    else:
        response = {
            'message': 'Our chain is authoritative',
            'chain': serializer.serialize_array(blockchain.chain)
        }

    return jsonify(response), 200


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-p', '--port', default=5000, type=int, help='port to listen on')
    args = parser.parse_args()
    port = args.port

    app.run(host='0.0.0.0', port=port)
