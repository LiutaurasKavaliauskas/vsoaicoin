class Account:
    balance = 0

    def __init__(self, balance):
        self.balance = balance

    @staticmethod
    def fetch(blockchain, public_key):
        balance = 0
        for block in blockchain.chain:
            for transaction in block.transactions:
                if transaction.sender == public_key:
                    balance -= float(transaction.amount)
                elif transaction.recipient == public_key:
                    balance += float(transaction.amount)

        for transaction in blockchain.current_transactions:
            if transaction.sender == public_key:
                balance -= float(transaction.amount)
            elif transaction.recipient == public_key:
                balance += float(transaction.amount)

        return Account(balance)
