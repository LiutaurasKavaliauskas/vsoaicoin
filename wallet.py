import hashlib
import string
from random import *
import requests


class Wallet:
    address = None
    balance = None
    transactions = []

    def __init__(self, address):
        wallet_data = requests.get('https://api.myjson.com/bins/nikhy', {'address': address}).json()

        self.address = address
        self.balance = wallet_data['balance']
        self.transactions = wallet_data['transactions']

    @staticmethod
    def generate_private_key():
        allchar = string.ascii_letters + string.punctuation + string.digits
        ramdom_char_table = "".join(choice(allchar) for x in range(randint(32, 64)))
        passphrase = ''
        for letter in ramdom_char_table:
            passphrase = passphrase + hashlib.sha256(letter.encode('utf-8')).hexdigest() + ' '

        return hashlib.sha256(passphrase.encode('utf-8')).hexdigest()

    @staticmethod
    def generate_public_key(private_key):
        return hashlib.sha256(private_key.encode('utf-8')).hexdigest()

    @staticmethod
    def access_wallet(address):
        return Wallet(address)

