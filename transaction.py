import datetime as date


class Transaction:
    """
    Class for the transaction

    Attributes:
        sender (str): sender public key
        recipient (str): recipient public key
        amount (float): amount of the coin
    """

    def __init__(self, sender, recipient, amount):
        self.sender = sender
        self.recipient = recipient
        self.amount = amount
        self.timestamp = date.datetime.now()
