import requests
from flask import url_for

from node import *

"""
Do the endless loop for the mining process
"""

recipient = input('Enter your public key: ')

while True:
    last_block = blockchain.last_block

    proof = blockchain.proof_of_work(last_block.proof, last_block.previous_hash)

    data = {
        'proof': proof,
        'recipient': recipient
    }

    response = requests.post(url='http://localhost:5000/block/new', data=data).json()

    print('\n------------------------------\n')
    print('index:', response['index'])
    print('proof:', response['proof'])
    print('\n------------------------------\n')
