import hashlib
import json
from urllib.parse import urlparse

import requests

import config
from account import Account
from block import Block
from insufficient_funds_exception import InsufficientFundsException
from transaction import Transaction


class Blockchain:
    """
    Class for the blockchain

    Attributes:
        chain (list): a chain of blocks
        current_transactions (list): currently made transactions to be added to the now block
        nodes (set): a set of nodes
    """

    def __init__(self):
        self.chain = [Block.create_genesis_block()]
        self.current_transactions = []
        self.nodes = set()

    def new_block(self, proof, recipient):
        """
        Create a new block

        :param proof: proof of work that was found
        :param recipient: recipient for the transaction to be made
        :return: a new block for the blockchain
        """
        self.new_transaction(
            sender="0",
            recipient=recipient,
            amount=1,
        )

        block = Block(
            len(self.chain),
            self.chain[-1].hash,
            proof,
            self.current_transactions
        )

        self.current_transactions = []

        self.chain.append(block)

        return block

    def new_transaction(self, sender, recipient, amount):
        """
        Create a new transaction

        :param sender: transaction sender
        :param recipient: transaction recipient
        :param amount: amount which is sent
        :return: new index
        """

        if sender != "0" and Account.fetch(self, sender).balance < int(amount):
            raise InsufficientFundsException('Insufficient funds!')

        self.current_transactions.append(Transaction(sender, recipient, amount))

        return self.chain[-1].index + 1

    def proof_of_work(self, last_proof, previous_hash):
        """
        Do the proof of work while it is not valid

        :param last_proof: last proof of work
        :param previous_hash: previous hash - used for the proof of work
        :return: a new proof that was found
        """
        proof = last_proof + 1

        while not self.valid_proof(last_proof, proof, previous_hash):
            proof += 1

        return proof

    @staticmethod
    def valid_proof(last_proof, proof, previous_hash):
        """
        Valid if the given proof is correct

        :param last_proof: last valid proof
        :param proof: a proof to be tried
        :param previous_hash: previous block hash
        :return: True is proof is correct otherwise False
        """
        guess = f'{20*last_proof**2 + 3*proof**2}{previous_hash}'.encode()
        guess_hash = hashlib.sha256(guess).hexdigest()

        return guess_hash[config.PROOF_OF_WORK['hash_level']:] == "888888"

    @property
    def last_block(self):
        """
        Get the last block of the blockchain

        :return: last block
        """
        return self.chain[-1]

    def hash(self, block):
        """
        Hash the block

        :param block: block
        :return: a hash string
        """
        return hashlib.sha256(json.dumps(block, sort_keys=True).encode()).hexdigest()

    def register_node(self, address):
        """
        Register a new node in the current set of nodes

        :param address: node address
        """
        parsed_url = urlparse(address)

        if parsed_url.netloc:
            self.nodes.add(parsed_url.netloc)
        elif parsed_url.path:
            self.nodes.add(parsed_url.path)
        else:
            raise ValueError('Invalid URL')

    def valid_chain(self, chain):
        """
        Check if the given chain is valid

        :param chain: given chain
        :return:
        """
        first_block = chain[0]
        current_index = 1

        while current_index < len(chain):
            block = chain[current_index]
            print(f'{first_block}')
            print(f'{block}')
            print("\n-----------\n")

            if block['previous_hash'] != first_block['hash']:
                return False

            if not self.valid_proof(first_block['proof'], block['proof'], block['previous_hash']):
                return False

            first_block = block
            current_index += 1

        return True

    def resolve_conflicts(self):
        """
        Resolve conflicts for the blockchain

        :return: True if a chain was changed otherwise False
        """
        neighbours = self.nodes
        new_chain = None

        max_length = len(self.chain)

        for node in neighbours:
            response = requests.get(requests.get(f'http://{node}').url + 'chain')

            if response.status_code == 200:
                length = response.json()['length']
                chain = response.json()['chain']

                if length > max_length and self.valid_chain(chain):
                    max_length = length
                    new_chain = chain

        if new_chain:
            for block in new_chain:
                self.chain.append(Block(block['index'], block['previous_hash'], block['proof'], block['transactions']))

            return True

        return False
