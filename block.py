import hashlib as hasher
import datetime as date


class Block:
    """
    Class for the blockchain block

    Attributes:
        index (int): index of the block
        previous_hash (str): previous block hash
        proof (int): proof of work found value
        transactions (list): transactions to be added
    """

    def __init__(self, index, previous_hash, proof, transactions):
        self.index = index
        self.previous_hash = previous_hash
        self.proof = proof
        self.timestamp = date.datetime.now()
        self.transactions = transactions
        self.hash = self.hash_block()

    def hash_block(self):
        """
        Hash this block data

        :return: Hash of the block data
        """
        sha = hasher.sha256()

        sha.update((str(self.index) +
                    str(self.previous_hash) +
                    str(self.proof) +
                    str(self.timestamp) +
                    str(self.transactions)).encode())

        return sha.hexdigest()

    @staticmethod
    def create_genesis_block():
        """
        Create the first block of a blockchain

        :return: A new block
        """
        return Block(0, "0", 3796323, [])
